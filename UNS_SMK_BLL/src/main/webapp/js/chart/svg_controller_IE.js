var NS="http://www.w3.org/2000/svg";
var draw;
var background_img;
var marker;
var imgPath = "../images/DashBoard/";

var pieChart1;
var pieChart2;
var borderColor = "";
var totalOperationRatio = 0;

$(function() {
	pieChart1 = $("#pieChart1").highcharts();
	pieChart2 = $("#pieChart2").highcharts();

	$("#title_main").css({
		left : window.innerWidth/2 - ($("#title_main").width()/2)
	});
	
	draw = SVG("svg");
	
	//getMarker();
	
	background_img = draw.image(imgPath+"Road.svg").size(contentWidth* 1.2, contentHeight);
	background_img.x(getElSize(-400));
	background_img.y(contentHeight/(targetHeight/-70));
	
//	var logoText =  draw.text(function(add) {
//		  add.tspan("　　　성역 없는 ").fill("#313131");
//		  add.tspan("혁신!").fill("#ff0000");
//		  add.tspan("전원이 참여하는 ").fill("#0005D5").newLine();
//		  add.tspan("혁신!").fill("#ff0000");
//		  add.tspan("　　나로 부터의 ").fill("#c00000").newLine();
//		  add.tspan("혁신!").fill("#ff0000");
//		  
//	});
//		
//	logoText.font({
//			'font-family':'나눔고딕'
//			, size:     75
//			, anchor:   'right'
//			, leading:  '1.3em'
//		   ,'font-weight':'bolder',
//	});
//	
//	logoText.x(2600);
//	logoText.y(1730);

	//id, width, height, w, h, viewBox, d, transform
	svg("SVG_1", getElSize(127.86), getElSize(54.955), getElSize(380),getElSize(220),"18.24 18.143 127.86 54.955", "M127.86,65.21v25.13c0,0.55-0.45,1-1,1h-10.91c-0.55,0-1,0.45-1,1v7.8c0,0.551-0.45,1-1,1h-7.92" + 
					"c-0.55,0-1,0.45-1,1v3.061c0,0.55-0.45,1-1,1H59.44c-0.55,0-1-0.45-1-1v-3.061c0-0.55-0.45-1-1-1H22.96c-0.55,0-1-0.449-1-1" + 
					"v-7.8c0-0.55-0.45-1-1-1H1c-0.55,0-1-0.45-1-1v-23.02c0-0.55,0.45-1,1-1h23.52c0.55,0,1-0.45,1-1v-8.21c0-0.55,0.45-1,1-1h0.76" + 
					"c0.55,0,1,0.45,1,1v2.35c0,0.55,0.45,1,1,1h21.75c0.55,0,1.24-0.38,1.521-0.85L57.19,52c0.29-0.47,0.96-0.8,1.5-0.75" + 
					"c0.54,0.06,0.99,0.1,0.99,0.1s0.01,0.01,0.02,0.02l-0.01-0.02l0.45,0.05v0.17c0-0.13,0.45-0.23,1-0.23h45.27c0.55,0,1,0.45,1,1" + 
					"v10.87c0,0.55,0.45,1,1,1h18.45C127.41,64.21,127.86,64.66,127.86,65.21z","translate(18.24,-33.1023)", "MCM25","noConn","Trumpf");
	
	svg("SVG_2", getElSize(134.02), getElSize(83.6), getElSize(400),getElSize(300),"18.243 18.241 134.02 83.62", "M63.777,37.481v81.62c0,0.55-0.45,1-1,1H0.996c-0.55,0-1-0.45-1-1v-10.21h-46.57c-0.55,0-1-0.45-1-1"+
					"v-13.3h-21.67c-0.55,0-1-0.45-1-1v-30.6c0-0.551,0.45-1,1-1h40.9v-4.67c0-0.55,0.45-1,1-1h9.12v-7.5c0-0.55,0.45-1,1-1h17.22"+
					"v-10.34c0-0.55,0.45-1,1-1h61.78C63.326,36.481,63.777,36.93,63.777,37.481z","translate(88.4864,-18.24)", "HF7P","wait", "Shenyang Group");
	
	svg("SVG_3", getElSize(160.32), getElSize(66.3), getElSize(530),getElSize(289),"18.237 18.238 160.32 66.38", "M81.208,68.171c0.55,0,1,0.45,1,1v37.18c0,0.55-0.45,1-1,1h-50.44c-0.55,0-1,0.45-1,1v3.66"+
					"c0,0.55-0.45,1-1,1h-12.18c-0.55,0-1,0.45-1,1v2.261c0,0.55-0.45,1-1,1h-2.25c-0.55,0-1,0.45-1,1v2.25c0,0.55-0.45,1-1,1h-17.21"+
					"c-0.55,0-1-0.45-1-1v-12.171c0-0.55-0.45-1-1-1h-68.24c-0.55,0-1-0.45-1-1v-37.18c0-0.55,0.45-1,1-1h76.11c0.55,0,1-0.45,1-1"+
					"v-11.03c0-0.55,0.45-1,1-1h32.02c0.55,0,1,0.45,1,1v11.03c0,0.55,0.45,1,1,1H81.208z","translate(96.3495,-36.9029)", "DCM37100F","wait", "Amada");
	
	svg("SVG_4", getElSize(124.72), getElSize(59.8), getElSize(400),getElSize(240),"18.242 18.239 124.72 59.8", "M123.723,63.223c0.55,0,1,0.45,1,1v31.06c0,0.55-0.45,1-1,1h-9.341c-0.55,0-1,0.45-1,1v10.57" +
					"c0,0.55-0.45,1-1,1H1.002c-0.55,0-1-0.45-1-1v-57.8c0-0.55,0.45-1,1-1h36.27c0.55,0,1,0.45,1,1v0.95c0,0.55,0.45,1,1,1h30.59" +
					"c0.55,0,1,0.45,1,1v0.95c0,0.55,0.45,1,1,1h27.77c0.55,0,1,0.45,1,1v6.27c0,0.55,0.45,1,1,1H123.723z","translate(18.24,-30.8137)", "5FPM","wait", "DMTG");
	
	svg("SVG_5", getElSize(158.74), getElSize(49.02), getElSize(500),getElSize(250),"18.24 18.24 158.74 49.02", "M92.543,36.48c0.55,0,1,0.45,1,1v39.979c0,0.55-0.45,1-1,1h-15.01c-0.55,0-1-0.45-1-1v-7.67" + 
					"c0-0.55-0.45-1-1-1h-38.53c-0.55,0-1.318,0.318-1.707,0.708l-6.986,6.994c-0.389,0.39-0.728,0.688-0.752,0.663" + 
					"s-0.495-0.045-1.045-0.045h-7.09c-0.55,0-1,0.45-1,1v6.39c0,0.55-0.45,1-1,1H1.003c-0.55,0-1-0.45-1-1V74.77c0-0.55-0.45-1-1-1" + 
					"h-12.18c-0.55,0-1-0.45-1-1v-2.98c0-0.55-0.45-1-1-1h-49.02c-0.55,0-1-0.45-1-1v-14.34c0-0.55,0.45-1,1-1h63.48" + 
					"c0.55,0,1-0.45,1-1V37.48c0-0.55,0.45-1,1-1H92.543z","translate(83.4369,-18.24)", "BG20","wait", "Komatsu");
	
	svg("SVG_6", getElSize(175.74), getElSize(64.4), getElSize(550),getElSize(250),"18.244 18.241 175.74 64.4", "M168.53,54.112c0.55,0,1,0.45,1,1v27.76c0,0.55-0.45,1-1,1h-68.86c-0.55,0-1,0.45-1,1v15.01"+
					"c0,0.55-0.45,1-1,1H-5.211c-0.55,0-1-0.45-1-1v-62.4c0-0.55,0.45-1,1-1h99.03c0.55,0,1,0.45,1,1v4.43c0,0.55,0.45,1,1,1h35.859"+
					"c0.55,0,1,0.45,1,1v9.2c0,0.55,0.45,1,1,1H168.53z","translate(24.4546,-18.24)", "BG40","wait", "DMG Mori Seiki A.G.");

	svg("SVG_7", getElSize(100.84), getElSize(34.08), getElSize(400),getElSize(200),"18.236 18.245 100.84 34.08", "M95.135,44.535c0.55,0,1,0.45,1,1v24.03c0,0.55-0.45,1-1,1h-98.84c-0.55,0-1-0.45-1-1v-2.771"+
					"c0-0.55,0.45-1,1-1h2.71c0.55,0,1-0.45,1-1v-27.31c0-0.55,0.45-1,1-1h20.43c0.55,0,1,0.45,1,1v6.05c0,0.55,0.45,1,1,1h19.26"+
					"c0.55,0,1-0.45,1-1v-6.05c0-0.55,0.45-1,1-1h20.44c0.55,0,1,0.45,1,1v6.05c0,0.55,0.45,1,1,1H95.135z","translate(22.9416,-18.24)", "BG30","wait", "DMG Mori Seiki Co. Ltd.");

	svg("SVG_8", getElSize(43), getElSize(24.221), getElSize(160),getElSize(77),"0 0 43 24.221", "M0,1.48c0-0.55,0.45-1,1-1h40.52c0.55,0,1,0.45,1,1v21.74c0,0.55-0.45,1-1,1H1c-0.55,0-1-0.45-1-1V1.48z","translate(0.24,-0.24)", "AH8J","wait", "Schuler");
	
	svg("SVG_9", getElSize(43), getElSize(24.221), getElSize(150),getElSize(72),"0 0 43 24.221", "M0,1.48c0-0.55,0.45-1,1-1h40.52c0.55,0,1,0.45,1,1v21.74c0,0.55-0.45,1-1,1H1c-0.55,0-1-0.45-1-1V1.48z","translate(0.24,-0.24)", "AH9J","wait", "Jtekt");
	
	svg("SVG_10", getElSize(72.56), getElSize(42.34), getElSize(230),getElSize(370),"18.244 18.235 72.56 42.34", "M71.564,53.865c0.55,0,1,0.45,1,1v7.64c0,0.55-0.45,1-1,1h-6.5c-0.55,0-1,0.45-1,1v8.68"+
				"c0,0.55-0.45,1-1,1h-6.5c-0.55,0-1,0.45-1,1v0.26c0,0.55-0.45,1-1,1h-7.07c-0.55,0-1,0.45-1,1v0.36c0,0.55-0.45,1-1,1h-18.41"+
				"c-0.55,0-1,0.45-1,1v5.38c0,0.55-0.45,1-1,1h-13.31c-0.55,0-1-0.45-1-1v-1.97c0-0.55-0.318-1.318-0.707-1.707l-1.996-1.996"+
				"c-0.389-0.389-1.157-0.707-1.707-0.707h-5.36c-0.55,0-1-0.45-1-1v-18.97c0-0.55,0.45-1,1-1h2.53c0.55,0,1.318-0.318,1.707-0.707"+
				"l7.276-7.276c0.389-0.389,0.707-1.157,0.707-1.707v-0.46c0-0.55,0.45-1,1-1h8.59c0.55,0,1-0.45,1-1v-0.84c0-0.55,0.45-1,1-1"+
				"h17.27c0.55,0,1,0.45,1,1v8.02c0,0.55,0.45,1,1,1H71.564z","translate(18.24,-25.6101)", "HM1250W","wait", "Okuma");
	
	svg("SVG_11", getElSize(70.865), getElSize(24.221), getElSize(230),getElSize(170),"18.24 18.24 70.865 42.52", "M0,37.48c0-0.55,0.45-1,1-1h68.865c0.55,0,1,0.45,1,1V78c0,0.55-0.45,1-1,1H1c-0.55,0-1-0.45-1-1V37.48z","translate(18.24,-18.24)", "NB13W" ,"inCycle", "MAG");
	
	svg("SVG_12", getElSize(70.865), getElSize(24.221), getElSize(230),getElSize(170),"18.24 18.24 70.865 42.52", "M0,37.48c0-0.55,0.45-1,1-1h68.865c0.55,0,1,0.45,1,1V78c0,0.55-0.45,1-1,1H1c-0.55,0-1-0.45-1-1V37.48z","translate(18.24,-18.24)", "NB14W","inCycle","Makino");
	
	svg("SVG_13", getElSize(43), getElSize(74.748), getElSize(270),getElSize(360),"0 0 74.748 96.008", "M0,37.48c0-0.55,0.45-1,1-1h36.268c0.55,0,1,0.45,1,1v57.527c0,0.55-0.45,1-1,1H1c-0.55,0-1-0.45-1-1" + 
					"V37.48z","translate(18.24,-18.24)", "5FMS#3","inCycle" ,"Doosan<br> Infracore");
	
	svg("SVG_14", getElSize(43), getElSize(74.748), getElSize(270),getElSize(360),"0 0 74.748 96.008", "M0,37.48c0-0.55,0.45-1,1-1h36.268c0.55,0,1,0.45,1,1v57.527c0,0.55-0.45,1-1,1H1c-0.55,0-1-0.45-1-1" + 
					"V37.48z","translate(18.24,-18.24)", "5FMS#2","inCycle", "Haas");
	
	svg("SVG_15", getElSize(43), getElSize(74.748), getElSize(270),getElSize(360),"0 0 74.748 96.008", "M0,37.48c0-0.55,0.45-1,1-1h36.268c0.55,0,1,0.45,1,1v57.527c0,0.55-0.45,1-1,1H1c-0.55,0-1-0.45-1-1" + 
					"V37.48z","translate(18.24,-18.24)", "5FMS#1","inCycle", "GF Mach'g<br>Solutions");
	
	svg("SVG_16", getElSize(25.512), getElSize(31.18), getElSize(80),getElSize(130),"18.24 18.24 25.512 31.18", "M0,37.48c0-0.551,0.449-1,1-1h23.512c0.55,0,1,0.449,1,1v29.18c0,0.55-0.45,1-1,1H1c-0.551,0-1-0.45-1-1"+
					"V37.48z", "translate(18.24,-18.24)", "HFMS<br>#1","inCycle", "Grob");
	
	svg("SVG_17", getElSize(25.512), getElSize(31.18), getElSize(80),getElSize(130),"18.24 18.24 25.512 31.18", "M0,37.48c0-0.551,0.449-1,1-1h23.512c0.55,0,1,0.449,1,1v29.18c0,0.55-0.45,1-1,1H1c-0.551,0-1-0.45-1-1"+
			"V37.48z", "translate(18.24,-18.24)", "HFMS<br>#2","inCycle", "Emag");
	
	svg("SVG_19", getElSize(25.512), getElSize(31.18), getElSize(80),getElSize(130),"18.24 18.24 25.512 31.18", "M0,37.48c0-0.551,0.449-1,1-1h23.512c0.55,0,1,0.449,1,1v29.18c0,0.55-0.45,1-1,1H1c-0.551,0-1-0.45-1-1"+
			"V37.48z", "translate(18.24,-18.24)", "HFMS<br>#3","inCycle", "United<br>Grinding");
	
	svg("SVG_20", getElSize(25.512), getElSize(31.18), getElSize(80),getElSize(130),"18.24 18.24 25.512 31.18", "M0,37.48c0-0.551,0.449-1,1-1h23.512c0.55,0,1,0.449,1,1v29.18c0,0.55-0.45,1-1,1H1c-0.551,0-1-0.45-1-1"+
			"V37.48z", "translate(18.24,-18.24)", "HFMS<br>#4","inCycle", "Aida");
	
	svg("SVG_21", getElSize(92.402), getElSize(45.354), getElSize(400),getElSize(130),"18.244 18.555 92.402 45.354", "M92.405,68.066c0.18,12.53-9.84,22.82-22.36,22.99c-5.77,0.08-11.07-2-15.12-5.5v2.35h-15.74v-6.07H0.004"+
			"v-34.02h18.7v8.51h20.48v-8.1h15.74v2.99c4.05-3.5,9.351-5.59,15.12-5.51C82.325,45.876,92.235,55.796,92.405,68.066z", "translate(18.24,-27.1489)", "AH20-6P","inCycle", "Gleason");
	
	svg("SVG_22", getElSize(106.44), getElSize(58.1), getElSize(350),getElSize(250),"18.24 18.245 106.44 58.1", "M105.44,54.91c0.55,0,1,0.45,1,1v37.68c0,0.55-0.45,1-1,1h-3.67c-0.55,0-1,0.45-1,1v16.42"+
					"c0,0.55-0.45,1-1,1H1c-0.55,0-1-0.45-1-1v-56.1c0-0.55,0.45-1,1-1H105.44z", "translate(18.24,-36.6652)", "HF3M","inCycle", "Hyundai WIA");
	
	svg("SVG_23", getElSize(86.457), getElSize(25.512), getElSize(250),getElSize(130),"18.24 18.24 86.457 25.512", "M0,37.48c0-0.55,0.45-1,1-1h84.457c0.55,0,1,0.45,1,1v23.512c0,0.55-0.45,1-1,1H1c-0.55,0-1-0.45-1-1" + 
					"V37.48z", "translate(18.24,-18.24)", "PROTH","inCycle", "Index");
	
	svg("SVG_24", getElSize(97.08), getElSize(38.26), getElSize(350),getElSize(180),"18.243 18.244 97.08 38.26", "M96.083,44.988c0.55,0,1,0.45,1,1v34.85c0,0.55-0.45,1-1,1h-10.75c-0.55,0-1-0.45-1-1v-5.09"+
					"c0-0.55-0.45-1-1-1h-29.18c-0.55,0-1,0.45-1,1v6.5c0,0.55-0.45,1-1,1h-12.18c-0.55,0-1-0.45-1-1v-6.5c0-0.55-0.45-1-1-1H1.003"+
					"c-0.55,0-1-0.45-1-1v-16.42c0-0.55,0.45-1,1-1h36.97c0.55,0,1-0.45,1-1v-9.34c0-0.55,0.45-1,1-1H96.083z", "translate(18.24,-26.7439)", "BG10","inCycle", "Mitsubishi H.I.");
	
	svg("SVG_25", getElSize(128.98), getElSize(49.72), getElSize(400),getElSize(220),"18.238 18.244 128.98 49.72", "M127.979,56.327c0.55,0,1,0.45,1,1v32.129c0,0.55-0.45,1-1,1h-57.53c-0.55,0-1,0.45-1,1v13.591"+
					"c0,0.55-0.45,1-1,1h-32.01c-0.55,0-1-0.45-1-1v-6.5c0-0.55-0.45-1-1-1h-9.34c-0.55,0-1-0.45-1-1v-9.341c0-0.55-0.45-1-1-1h-22.1"+
					"c-0.55,0-1-0.45-1-1V63.117c0-0.55,0.45-1,1-1h34.85c0.55,0,1-0.45,1-1v-3.79c0-0.55,0.45-1,1-1H127.979z", "translate(18.24,-38.0825)", "DCM2780F","inCycle", "Bystronic");
	
	svg("SVG_26", getElSize(167.527), getElSize(52.44), getElSize(900),getElSize(160),"18.24 18.241 167.527 52.44", "M93.823,72.915v26.35c0,0.55-0.45,1-1,1h-65.75v11.75c0,0.55-0.45,1-1,1h-98.77c-0.55,0-1-0.45-1-1"+
					"v-36.27c0-0.551,0.45-1,1-1h72.7v-13.17c0-0.55,0.45-1,1-1h36.41c0.55,0,1,0.45,1,1v10.34h54.41"+
					"C93.372,71.915,93.823,72.365,93.823,72.915z", "translate(91.9408,-42.3345)", "HF7M","inCycle", "Heller");
	
	svg("SVG_27", getElSize(31.18), getElSize(59.527), getElSize(130),getElSize(230),"0.24 0.24 31.18 59.527", "M0,1.48c0-0.55,0.45-1,1-1h29.18c0.55,0,1,0.45,1,1v57.527c0,0.55-0.45,1-1,1H1c-0.55,0-1-0.45-1-1V1.48"+
					"z", "translate(0.24,-0.24)", "YBM<br>1530V","inCycle", "Rofin<br>Sinar");
	
	svg("SVG_28", getElSize(34.016), getElSize(69.449), getElSize(130),getElSize(270),"18.24 18.24 34.016 69.449", "M33.018,93.168c0.55,0,1,0.45,1,1v22.1c0,0.55-0.45,1-1,1h-0.84c-0.55,0-1,0.45-1,1v5.09"+
					"c0,0.55-0.45,1-1,1h-0.83c-0.55,0-1,0.45-1,1v9.33c0,0.55-0.45,1-1,1h-6.51c-0.55,0-1-0.45-1-1v-9.33c0-0.55-0.45-1-1-1h-6.5"+
					"c-0.55,0-1-0.45-1-1v-0.84c0-0.55-0.45-1-1-1h-2.25c-0.55,0-1-0.45-1-1v-13.59c0-0.55-0.45-1-1-1h-5.09c-0.55,0-1-0.45-1-1"+
					"v-37.68c0-0.55,0.45-1,1-1h29.18c0.55,0,1,0.45,1,1v24.92c0,0.55,0.45,1,1,1H33.018z", "translate(18.24,-48.0038)", "HM2J","inCycle" ,"Nachi<br>Fujikoshi");
	
	svg("SVG_29", getElSize(36.72), getElSize(60.94), getElSize(130),getElSize(250),"0.244 0.242 36.72 60.94", "M35.724,45.834c0.55,0,1,0.45,1,1v27.76c0,0.55-0.45,1-1,1h-3.67c-0.55,0-1,0.45-1,1v15.01"+
			"c0,0.55-0.45,1-1,1H9.384c-0.55,0-1-0.45-1-1v-22.09c0-0.55-0.45-1-1-1h-0.84c-0.55,0-1-0.45-1-1v-5.09c0-0.55-0.45-1-1-1h-3.54"+
			"c-0.55,0-1-0.45-1-1v-27.76c0-0.55,0.45-1,1-1h29.05c0.55,0,1,0.45,1,1v12.17c0,0.55,0.45,1,1,1H35.724z", "translate(0.24,-31.4211)", "HM1J","inCycle", "Brother");
	
	svg("SVG_30", getElSize(45.834), getElSize(33.078), getElSize(170),getElSize(140),"0 0 45.834 33.078", "M0,1.48c0-0.55,0.45-1,1-1h43.354c0.55,0,1,0.45,1,1v30.598c0,0.55-0.45,1-1,1H1c-0.55,0-1-0.45-1-1"+
					"V1.48z", "translate(0.24,-0.24)", "AH1J","inCycle", "Sodick");
	
	svg("SVG_31", getElSize(34.496), getElSize(47.961), getElSize(130),getElSize(250),"0 0 34.496 47.961", "M0,1.48c0-0.55,0.45-1,1-1h32.016c0.55,0,1,0.45,1,1v45.48c0,0.55-0.45,1-1,1H1c-0.55,0-1-0.45-1-1V1.48"+
					"z", "translate(0.24,-0.24)", "AH2J","inCycle", "Prima");
	
	svg("SVG_36", getElSize(76.68), getElSize(38.26), getElSize(280),getElSize(120),"18.239 18.244 76.68 38.26", "M75.678,44.984c0.55,0,1,0.45,1,1v17.84c0,0.55-0.45,1-1,1h-3.67c-0.55,0-1,0.45-1,1v7.92"+
					"c0,0.55-0.45,1-1,1H0.999c-0.55,0-1-0.45-1-1v-32.01c0-0.55,0.45-1,1-1h27.91c0.55,0,1-0.45,1-1v-2.25c0-0.55,0.45-1,1-1h24.93"+	
					"c0.55,0,1,0.45,1,1v6.5c0,0.55,0.45,1,1,1H75.678z", "translate(18.24,-18.24)", "HM1250-6P","inCycle" ,"Starrag");
	
//	svg("SVG_37", getElSize(7.77819), getElSize(0.797778), getElSize(880),getElSize(100),"0 0 560.03 57.44", "M559.28 29.09 L545.19 29.09 L545.19 0.75 L521.05 0.75 L521.05 29.09 L512 29.09 L512 0.75 L488.86 0.75 L488.86"+
//						 "29.09 L476.79 29.09 L476.79 13.91 L459.69 13.91 L459.69 28.08 L404.37 28.08 L404.37 13.91 L10.06 13.91 L10.06"+
//						 "27.07 L0 27.07 L0 57.44 L559.28 57.44 L559.28 29.09 Z", "translate(0.374999,-0.374999)", "");
	

	
	getMachineInfo();
	//getMachineInfo_Spark();
});

function getMachineInfo_Spark(){
	var url = ctxPath + "/spark/getMachinePos.do";
	
	$.ajax({
		url : url,
		dataType : "text",
		type : "post",
		success :function(data){
			
		}
	});
};

function alarmTimer(){
	setInterval(function(){
		if(s2==10){
    		s++;
    		$("#second1").html(s);
    		s2 = 0;	
    	};
    	
    	if(s==6){
    		s = 0;
    		m2++;
    		
    		$("#second1").html(s);
    		$("#minute2").html(m2);
    	};
    	
    	if(m2==10){
    		m2 = 0;
    		m++;
    		$("#minute1").html(m);
    	};
		
    	$("#second2").html(s2);
    	s2++;
    	
	},1000);
};

function redrawPieChart(){
	var url = ctxPath + "/svg/getMachineInfo2.do";
	
	$.ajax({
		url : url,
		type : "post",
		dataType : "json",
		success : function (data){
			var json = data.machineList;

			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;
			
			$(json).each(function(key, data){
				if(data.lastChartStatus=="IN-CYCLE"){
					inCycleMachine++;
				}else if(data.lastChartStatus=="WAIT"){
					waitMachine++;
				}else if(data.lastChartStatus=="ALARM"){
					alarmMachine++;
				}else if(data.lastChartStatus=="NO-CONNECTION"){
					powerOffMachine++;
				}
			});
			
			if(waitMachine==0 && alarmMachine==0){
				borderColor = "blue";
			}else if(alarmMachine==0){
				borderColor = "yellow";
			}else if(alarmMachine!=0){
				borderColor = "red";
			};
			bodyNeonEffect(borderColor);
			
			pieChart2.series[0].data[0].update(inCycleMachine);
			pieChart2.series[0].data[1].update(waitMachine);
			pieChart2.series[0].data[2].update(alarmMachine);
			pieChart2.series[0].data[3].update(powerOffMachine);
		}
	});
};

function printMachineName(arrayIdx, x, y, w, h, name, color, fontSize){
	var mName = draw.text(nl2br(machineName[arrayIdx])).fill(color);
	mName.font({
		  family:   'Helvetica'
		, size:     fontSize
		, anchor:   'middle'
		, leading:  '0em'
	});
	prit
	var wMargin = 0;
	var hMargin = 0;
	
	if(name=="SET UP"){
		hMargin = 30;
	}else if(name.indexOf("WASHING")!=-1){
		wMargin = 10;
	}else if(name.indexOf("NHM")!=-1){
		wMargin = -10;
	}
	
	mName.x(x + (w/2));
	mName.y(y + (h/2));
	mName.css("z-index","999")
	mName.leading(1);
};

function nl2br(value) {
	  return value.replace(/<br>/g, "\n");
};

var machineProp = new Array();
var machineList = new Array();
var machineStatus = new Array();
var newMachineStatus = new Array();
var machineArray2 = new Array();
var first = true;

var machineName = [
                   "Trumpf",
                   "Shenyang Group",
                   "Amada",
                   "DMTG",
                   "Komatsu",
                   "DMG Mori <br>Seiki A.G.",
                   "DMG Mori <br>Seiki Co. Ltd.",
                   "Schuler",
                   "Jtekt",
                   "Okuma",
                   "MAG",
                   "Makino",
                   "Doosan<br> Infracore",
                   "Haas",
                   "GF Mach'g <Br> Solutions",
                   "Grob",
                   "Emag",
                   "United<br> Grinding",
       				"Aida",
       				"Gleason",
       				"Hyundai<br> WIA",
       				"Index",
       				"Mitsubishi <br>H.I.",
       				"Bystronic",
       				"Heller",
       				"Rofin-Sinar",
       				"Nachi-Fujikoshi",
       				"Brother",
       				"Sodick",
       				"Prima",
       				"Starrag"
                   ];

function getMachineInfo(){
	var url = ctxPath + "/svg/getMachineInfo.do";
	$.ajax({
		url : url,
		type : "post",
		dataType : "json",
		success : function (data){
			var json = data.machineList;
			newMachineStatus = new Array();
			
			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;

			operationTime = 0;
			
			console.log(data)
			$(json).each(function(key, data){
				var array = new Array();
				var machineColor = new Array();
				
			//	if(data.name=="MCM25")svg(data.name, data.width, data.height, data.w, data.h, data.viewBox, data.d, data.transform);
				$("#SVG_" + data.id).css({
					"top" : getElSize(data.ieY),
					"left" : getElSize(data.ieX),
					"position" : "absolute",
					"opacity" :1
				});
				
				if(key<=30){
					//printMachineName(key, getElSize(data.ieX), getElSize(data.ieY), getElSize(data.w), getElSize(data.h), name, "white", getElSize(30))
				}
				
//				mName.x(x + (w/2));
//				mName.y(y + (h/2));
				
				$("#SVG_" + data.id + "_name").css({
					"top" : getElSize(data.ieY) +getElSize(data.h)/2 - getElSize(30),
					"left" : getElSize(data.ieX) + getElSize(data.w)/2 - getElSize(40),
					"position" : "absolute",
					"opacity" :1,
					"font-size" : getElSize(25)
				});
				
				array.push(data.id);
				array.push(replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"));
				array.push(data.x);
				array.push(data.y);
				array.push(data.w);
				array.push(data.h);
				array.push(data.pic);
				array.push(data.lastChartStatus);
				array.push(data.dvcId);
				array.push(data.fontSize);
				
				operationTime += Number(data.operationTime);
				
				if(data.lastChartStatus=="IN-CYCLE"){
					inCycleMachine++;
				}else if(data.lastChartStatus=="WAIT"){
					waitMachine++;
				}else if(data.lastChartStatus=="ALARM"){
					alarmMachine++;
				}else if(data.lastChartStatus=="NO-CONNECTION"){
					powerOffMachine++;
				};
				
				machineProp.push(array);
				/*machineProp.push("machine");
				machineProp.push("name");*/
				
				machineColor.push(data.id);
				machineColor.push(data.lastChartStatus);
				
				newMachineStatus.push(machineColor);
			});
			
			pieChart2.series[0].data[0].update(21);
			pieChart2.series[0].data[1].update(9);
			pieChart2.series[0].data[2].update(0);
			pieChart2.series[0].data[3].update(1);
			
//			pieChart2.series[0].data[0].update(inCycleMachine);
//			pieChart2.series[0].data[1].update(waitMachine);
//			pieChart2.series[0].data[2].update(alarmMachine);
//			pieChart2.series[0].data[3].update(powerOffMachine);
			
//			if(!compare(machineStatus,newMachineStatus)){
//				reDrawMachine();
//				redrawPieChart();
//			};
			
//			if(first){
//				for(var i = 0; i < machineProp.length; i++){
//					//array = id, name, x, y, w, h, pic, status, idx, dvcId, fintSize
//					drawMachine(machineProp[i][0],
//							machineProp[i][1],
//							machineProp[i][2],
//							machineProp[i][3],
//							machineProp[i][4],
//							machineProp[i][5],
//							machineProp[i][6],
//							machineProp[i][7],
//							i,
//							machineProp[i][8],
//							machineProp[i][9]);
//				};
//				first = false;
//			};
			
			
			//cal totalOperationRatio
			totalOperationRatio = 0;
			for(var i = 0; i < machineArray2.length; i++){
				totalOperationRatio += machineArray2[i][20];
			};
			
			var totalMachine = 0;
			totalMachine += (inCycleMachine + waitMachine + alarmMachine);
			var totalOperationRatio = Number(inCycleMachine / totalMachine * 100).toFixed(1);
//			pieChart1.series[0].data[0].update(Number(totalOperationRatio));
//			pieChart1.series[0].data[1].update(100-Number(totalOperationRatio));
		}
	});
	
	//setTimeout(getMachineInfo, 3000);
};

function replaceAll(str, src, target){
	return str.split(src).join(target)
};

function reDrawMachine(){
	for(var i = 0; i < machineStatus.length; i++){
		if(machineStatus[i][1]!=newMachineStatus[i][1]){
			machineList[i].remove();
			
			//array = id, name, x, y, w, h, pic, status
			
			drawMachine(machineProp[i][0],
					machineProp[i][1],
					machineProp[i][2],
					machineProp[i][3],
					machineProp[i][4],
					machineProp[i][5],
					machineProp[i][6],
					newMachineStatus[i][1],
					i,
					machineProp[i][8],
					machineProp[i][9]);
		}
	};
	
	machineStatus = newMachineStatus;
};

function compare ( a, b ){
	var type = typeof a, i, j;
	 
	if( type == "object" ){
		if( a === null ){
			return a === b;
		}else if( Array.isArray(a) ){ //배열인 경우
			//기본필터
	      if( !Array.isArray(b) || a.length != b.length ) return false;
	 
	      //요소를 순회하면서 재귀적으로 검증한다.
	      for( i = 0, j = a.length ; i < j ; i++ ){
	        if(!compare(a[i], b[i]))return false;
	      	};
	      return true;
	    };
	  };
	 
	  return a === b;
};

function getMarker(){
	var url = ctxPath + "/svg/getMarker.do";
	
	$.ajax({
		url : url,
		type: "post",
		dataType : "json",
		success : function(data){
			marker = draw.image(imgPath + data.pic + ".png").size(data.w, data.h);
			marker.x(data.x);
			marker.y(data.y);
			
			
			//marker.draggable();
			marker.dragmove  = function(delta, evt){
				var id = data.id;
				var x = marker.x();
				var y = marker.y();
				
				//DB Access
				setMachinePos(id, x, y);
			};
		}
	});
};

function drawMachine(id, name, x, y, w, h, pic, status, i, dvcId, fontSize){
	var svgFile;
	if(status==null){
		svgFile = ".svg";
	}else{
		svgFile = "-" + status + ".svg";
	};
	
	machineList[i] = draw.image(imgPath + pic + svgFile).size(w, h);
	machineList[i].x(x);
	machineList[i].y(y);
	
	var text_color;
	if(status=="WAIT" || status=="NO-CONNECTION"){
		text_color = "#000000";
	}else{
		text_color = "#ffffff";
	};
	
	//idx, x, y, w, h, name
	printMachineName(i, x, y, w, h, name, text_color, fontSize);
	
	//idx, machineId, w, h
	//setDraggable(i, id, w, h);
	
	machineList[i].dblclick(function(){
		alert("dvc Id=" + dvcId);
	})
	//alarm timer
	/*if(status=="ALARM"){
		drawAlarmTimer(x, y, w, h, id);
	};*/
};

function drawAlarmTimer(x, y, w, h, id){
	var timerDiv = $("<div id='timer'>"+
								"<table>"+
								"<tr>"+
									"<tD>"+
										"<div id='minute1-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD>"+
										"<div id='minute2-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD valign='middle'>"+
										"<font  style='font-size: 30px; font-weight:bolder;'>:</font>"+
									"</tD>"+
									"<tD>"+
										"<div id='second1-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD>"+
										"<div id='second2-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
								"</tr>"+
							"</table>"+
						"</div>");
	
	$(timerDiv).css({
		"position" : "absolute",
		//"width" : 200,
		"left" : x + (w/2) - (100),
		"top" : y + (h+5)
	});
	
	$("#odometerDiv").append(timerDiv);
};

function setDraggable(arrayIdx, id, w, h){
	//machineList[arrayIdx].draggable();
	
	machineList[arrayIdx].dragmove  = function(delta, evt){
		var x = machineList[arrayIdx].x();
		var y = machineList[arrayIdx].y();
		
		//setNamePos
		machineName[arrayIdx].x(x + (w/2) - 30);
		machineName[arrayIdx].y(y + (h/2) - 10);
		
		//DB Access
		setMachinePos(id, x, y);
	};
};

function setMachinePos(id, x, y){
	var url = ctxPath + "/svg/setMachinePos.do";
	var param = "id=" + id + 
					"&x=" + x + 
					"&y=" + y;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){}
	});
};

function svg(id, width, height, w, h, viewBox, d, transform, name, status, mName){
	var svg=document.createElementNS(NS,"svg");
	svg.setAttribute("id", id);
	svg.setAttribute("class", "svg");
	svg.setAttribute("width", width);
	svg.setAttribute("height", height);
	svg.setAttribute("viewBox", viewBox);
	svg.setAttribute("z-index", "-1");
	svg.setAttribute("opacity", "0"); 
	//svg.setAttribute("position", "absolute");
	
	var _svg = document.getElementById("svg");
	_svg.appendChild(svg);
	
	var color = "white";
	if(status=="wait") color="black";
	var mName = "<font style='position:absolute;color:" +color + "'; id=" + id + "_name>" + mName + "</font>";
//	//_svg.append(mName);
	$("#container").append(mName);
	
//	document.body.appendChild(svg);
	path(d, transform, w, h, id, status)
};

function path(d, transform, w, h, svg, status){
	var obj=document.createElementNS(NS,"path");
	obj.setAttribute("d", d);
	obj.setAttribute("transform", transform);
	obj.setAttribute("fill", "url(#"+status+")");
	
	document.querySelector('#' + svg).appendChild(eval("" + status + "()"));
	document.querySelector('#' + svg).appendChild(obj);
	
	scale(svg, w, h)
}

function inCycle(){
	var grad  = document.createElementNS(NS,'linearGradient');
	grad.setAttribute("id", "inCycle");
	grad.setAttribute("fy", "0");
	
	var stop  = document.createElementNS(NS,'stop');
	stop.setAttribute("offset", "0");
	stop.setAttribute("stop-color", "#148F01");
	
	var stop2  = document.createElementNS(NS,'stop');
	stop2.setAttribute("offset", "0.7");
	stop2.setAttribute("stop-color", "#46DF04");
	
	grad.appendChild(stop);
	grad.appendChild(stop2);
	
	return grad;
};

function wait(){
	var grad  = document.createElementNS(NS,'linearGradient');
	grad.setAttribute("id", "wait");
	grad.setAttribute("x1", "0%");
	grad.setAttribute("y1", "100%");
	
	var anim  = document.createElementNS(NS,'animate');
	anim.setAttribute("attributeName", "stop-color");
	anim.setAttribute("values", "#C7C402;#FEFE82;#C7C402");
	anim.setAttribute("dur", "500ms");
	anim.setAttribute("repeatCount", "indefinite");
	
	var stop  = document.createElementNS(NS,'stop');
	stop.setAttribute("offset", "0%");
	stop.setAttribute("stop-color", "#FEFE82");
	stop.setAttribute("stop-opacity", "1");
	
	stop.appendChild(anim);
	grad.appendChild(stop);
	
	return grad;
};

function alarm(){
	var grad  = document.createElementNS(NS,'linearGradient');
	grad.setAttribute("id", "alarm");
	grad.setAttribute("x1", "0%");
	grad.setAttribute("y1", "100%");
	
	var anim  = document.createElementNS(NS,'animate');
	anim.setAttribute("attributeName", "stop-color");
	anim.setAttribute("values", "red;#6B0202;red");
	anim.setAttribute("dur", "500ms");
	anim.setAttribute("repeatCount", "indefinite");
	
	var stop  = document.createElementNS(NS,'stop');
	stop.setAttribute("offset", "0%");
	stop.setAttribute("stop-color", "#FEFE82");
	stop.setAttribute("stop-opacity", "1");
	
	stop.appendChild(anim);
	grad.appendChild(stop);
	
	return grad;
};

function noConn(){
	var grad  = document.createElementNS(NS,'linearGradient');
	grad.setAttribute("id", "noConn");
	grad.setAttribute("fy", "0");
	
	var stop  = document.createElementNS(NS,'stop');
	stop.setAttribute("offset", "0");
	stop.setAttribute("stop-color", "#D7D7D7");
	
//	var stop2  = document.createElementNS(NS,'stop');
//	stop2.setAttribute("offset", "0.7");
//	stop2.setAttribute("stop-color", "#46DF04");
	
	grad.appendChild(stop);
	//grad.appendChild(stop2);
	
	return grad;
};

function scale(id, width, height){
	width *= 1.2
	$("#" + id).width(width)
	$("#" + id).height(height)
//	$("#" + id).draggable({
//		stop : function(){
//			var offset = $(this).offset();
//            var x = offset.left;
//            var y = offset.top;
//            
//            id = id.substr(id.indexOf("_")+1);
//            setMachinePosIE(id, x, y);
//		}
//	});
	
	id = id.substr(id.indexOf("_")+1);
	//getMachinePosIE(id);
};

function getMachinePosIE(id){
	var url = ctxPath + "/chart/getMachinePosIE.do";
	var param = "dvcId=" + id;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success :function(data){
			$("#SVG_" + id).css({
				"top" :getElSize(data.ieY),
				"left" : getElSize(data.ieX)
			})
		}
	});
};

function setMachinePosIE(id, x, y){
	var url = ctxPath + "/chart/setMachinePosIE.do";
	var param = "ieX=" + setElSize(x - $("#container").offset().left) + 
				"&ieY=" + setElSize(y) +
				"&id=" +  id;
	
	$.ajax({
		url : url,
		data :param,
		type : "post",
		success :function(){}
	});
};