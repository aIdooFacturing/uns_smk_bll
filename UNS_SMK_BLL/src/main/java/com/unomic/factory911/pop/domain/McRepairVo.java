package com.unomic.factory911.pop.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.unomic.dulink.common.domain.PagingVo;

@Getter
@Setter
@ToString
public class McRepairVo{

//	String jabcd;
//	String jobnm;
	String mcNo;
//	String mcName;
//	String usernm;
	String stDate;
	String fnDate;
	
	String repCause;
//	String prSort2;
//	String lmType;
	String ename;
	
//	int jobMin;
//	int lmSize;
//	int railSize;
	

}
