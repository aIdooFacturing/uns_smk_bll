package com.unomic.factory911.mc_prgm.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class McInfoVo{
	Integer ulkId;
	String ulkIp;
	String dvcId;
	String name;
	String mcNo;
	String hyung;
	Integer railSize;
	String mcType;
	Integer tgCnt;
}
