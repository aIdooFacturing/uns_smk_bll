package com.unomic.factory911.device.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class DeviceVo{
	String adtId;
	String dvcId;
	String adapterId;
	String lastUpdateTime;
	String lastStartDateTime;
	String lastChartStatus;
	Integer lastFeedOverride;
	Float lastSpdLoad;
	String lastAlarmCode;
	String lastAlarmMsg;
	String workDate;
	String lastProgramName;
	String lastProgramHeader;
	
	String lastCycleTime;
	
	String stopDate;
	String stopTime;
	
	//for night chart
	String targetDate;
	String nightStart;
	String nightEnd;
	String genStart;
	String genEnd;
	
	String stDate;
	String edDate;
	String tgDate;
	
	String ulkIp;
	String chartStatus;
	String startDateTime;
	
	String cycleCount;
	String cycleEndFiveMinutes;
	String cycleEndThreeMinutes;

	String avrCycleTime;
	String mcNo;
	
	Integer lastPartCount;
}
