package com.unomic.factory911.device.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class DeviceSpVo{
	String DVC_ID;
	String PARAM_CHART_STATUS;
  	String PARAM_SPD_LOAD;
  	String PARAM_FEED_OVERRIDE;
	String PARAM_PROGRAM_HEADER;
	String PARAM_PROGRAM_NAME;
	String PARAM_START_DATE_TIME;

	String PARAM_ALARM_MSG1;
	String PARAM_ALARM_MSG2;
	String PARAM_ALARM_MSG3;

	String PARAM_ALARM_NUM1;
	String PARAM_ALARM_NUM2;
	String PARAM_ALARM_NUM3;

	String PARAM_MODAL_M1;
	String PARAM_MODAL_M2;
	String PARAM_MODAL_M3;
	String last_ip_addr;
}
