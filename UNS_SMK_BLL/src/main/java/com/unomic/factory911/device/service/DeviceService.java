package com.unomic.factory911.device.service;

import java.util.List;

import com.unomic.factory911.adapter.domain.AdapterVo;
import com.unomic.factory911.device.domain.DeviceStatusVo;
import com.unomic.factory911.device.domain.DeviceVo;
import com.unomic.factory911.mc_prgm.domain.McInfoVo;



public interface DeviceService {
	public String editLastDvcStatus(AdapterVo pureVo);
	public String calcDeviceOptime(DeviceVo dvcVo);
	public String calcDvcOpPf(DeviceVo inputVo);
	public String calcDeviceTimes(DeviceVo dvcVo);
	public String calcDeviceTimesTest(DeviceVo dvcVo);
	public String addTimeChart(DeviceVo inputVo);
	public String addDvcSummary(DeviceVo inputVo);
	public String editDvcLastNoCon_SP();
	public String adjustDeviceChartStatus_SP(DeviceStatusVo inputVo);
	public String addDvcChartStatus(AdapterVo inputVo);
	public AdapterVo getIdfromIp(AdapterVo inputVo);
	public List<McInfoVo> getListMcInfo();
	public String addDvcStatics(DeviceVo inputVo);
	public String updateMcPrgmAvg();
	public String mqt();
	public String editDvcLastTime(String sender);
	public String addDvcDuple(String sender);
	
}
