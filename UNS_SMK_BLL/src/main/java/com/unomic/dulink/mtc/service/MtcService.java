package com.unomic.dulink.mtc.service;

import com.unomic.dulink.mtc.domain.MtcCtrlVo;
import com.unomic.dulink.mtc.domain.SerialVo;



public interface MtcService {
	public String setChgDvcCtrl(MtcCtrlVo inputVo);
	public void setJsonData(SerialVo inputVo) throws Exception;
	
}
