package com.unomic.dulink.mtc.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.unomic.dulink.common.domain.PagingVo;

@Getter
@Setter
@ToString
public class CycleVo{
	String mnPgmStDt;
	String mnPgmEdDt;
	int ttlPrgBlc;
	String mnPgmNm;
	String mnPgmHd;
	int partCnt;
	String regdt;
}


