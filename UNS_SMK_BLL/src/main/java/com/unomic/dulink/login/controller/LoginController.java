package com.unomic.dulink.login.controller;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.login.domain.LoginVo;
import com.unomic.dulink.login.domain.NcModelVo;
import com.unomic.dulink.login.service.LoginService;
/**
 * Handles requests for the application home page.
 */
@Controller
public class LoginController {

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	String demoId = "demo";
	String demoPW = "demo";
	
	@Autowired
	LoginService loginService;
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String home(Model model) {
		
		/* for register */
		List<NcModelVo> ncList = loginService.getNcManufacture();
		
		model.addAttribute("nc", ncList);
		
		//System.out.println(ncList.toString());
		model.addAttribute("seimensId", "Fanuc" );
		return "main/index";
	}
	
	@RequestMapping(value="submit")
	@ResponseBody
	public String submit(@RequestBody Map bdy){
		JSONObject obj = new JSONObject(bdy);
		String userId = obj.get("user").toString();
		String pw = obj.get("pw").toString();
		/*
		 * demo id / pw
		 * */
		String str = "fail";
		if(demoId.equals(userId) && demoPW.equals(pw)){
			str = "success";
		}
		return str;
	};
	
	/*@RequestMapping(value = "register", method = RequestMethod.GET)
	public String registerPage(Model model){
		 model : nc company name, model name list 
		
		List<String> cncList = new ArrayList<String>();
		cncList.add("Sinumeric 808D");
		cncList.add("Sinumeric 828D");
		cncList.add("Sinumeric 840D");
		
		model.addAttribute("simensId", "Siemens");
		model.addAttribute("Siemens", cncList);
		return "main/register";
	}*/
	
	@RequestMapping(value="getNCList")
	@ResponseBody
	public String getNCList() throws Exception{
		return loginService.getNCList();
	};
	
	@RequestMapping(value="login")
	@ResponseBody
	public String login(LoginVo loginVo){
		String str = "";
		try {
			str = loginService.login(loginVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="join")
	@ResponseBody
	public String join(LoginVo loginVo){
		String str = "";
		try {
			str = loginService.join(loginVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		return str;
	};
};

