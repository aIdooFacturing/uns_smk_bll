package com.unomic.dulink.program.domain;


import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class ProgramVo{
	Integer dvcId;
	Integer id;
	Integer prgmId;
	String name;
	Integer targetCnt;
	Integer currentCnt;
	String version;
	String condition;
}
